///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 4
///
/// @file list.hpp
/// @version 1.0
///
/// Custom made list
///
/// @author Colby Kagamida <colbykag@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   28_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

#include <string>

#include "node.hpp"

namespace animalfarm {

class SingleLinkedList {

public:
   //function members
   const bool empty() const;
   void push_front( Node* newNode );
   Node* pop_front();
   Node* get_first() const;
   Node* get_next( const Node* currentNode ) const;
   unsigned int size() const;

protected:
   Node* head = nullptr;

};

} // namespace animalfarm
