///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 06a - Animal Farm 3
///
/// @file random.hpp
/// @version 1.0
///
/// Generate Random numbers using LCG method
///
/// @author Colby Kagamida <colbykag@hawaii.edu>
/// @brief  Lab 06a - AnimalFarm3 - EE 205 - Spr 2021
/// @date   7_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

#include <string>

namespace animalfarm {

class Random {
public:

   Random();
   
   void setParams(unsigned int s, int mult, int incr, unsigned int modu);
   virtual int scgRand();
   
   unsigned int seed, m;
   int a, c;
};

} // namespace animalfarm
