///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file random.cpp
/// @version 1.0
///
/// Random number generator using linear congruential generation 
///
/// @author Colby Kagamida <colbykag@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   18_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "random.hpp"

using namespace std;

namespace animalfarm {

Random::Random(){
   a = 0;
   c = 0;
   seed = 0;
   m = 0;
}

void Random::setParams(unsigned int s, int mult, int incr, unsigned int modu) {
   seed = s;
   a = mult;
   c = incr;
   m = modu;
}

int Random::scgRand(){
   seed = (a * seed + c) % m;
   return seed;
}

} // namespace animalfarm
