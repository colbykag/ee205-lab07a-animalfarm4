///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file node.cpp
/// @version 1.0
///
/// Node
///
/// @author Colby Kagamida <colbykag@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   28_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#include "node.hpp"

using namespace std;

namespace animalfarm {

} // namespace animalfarm
