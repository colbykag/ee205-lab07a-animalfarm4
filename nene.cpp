///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author Colby Kagamida <colbykag@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   18_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "nene.hpp"

using namespace std;

namespace animalfarm {

Nene::Nene(string newTagID, enum Color newColor, enum Gender newGender ){
   
   tagID = newTagID;

   gender = newGender;

   species = "Branta sandvicensis";

   featherColor = newColor;

   isMigratory = true;
}

///Print Nene stuff then bird stuff

void Nene::printInfo() {
   cout << "Nene" << endl;
   cout << "   Tag ID = [" << tagID << "]" << endl;
   Bird::printInfo();
}

//override Bird::speak with Nene::speak
const string Nene::speak() {
   return string("Nay, nay");
}

}//namespace animalfarm
