///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file animal.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Colby Kagamida <colbykag@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   18_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "animal.hpp"

using namespace std;

namespace animalfarm {

Animal::Animal() {
   cout << '.';
}

Animal::~Animal() {
   cout << 'x';
}

void Animal::printInfo() {
	cout << "   Species = [" << species << "]" << endl;
	cout << "   Gender = [" << genderName( gender ) << "]" << endl;
}


string Animal::genderName (enum Gender gender) {
   switch (gender) {
      case MALE:    return string("Male"); break;
      case FEMALE:  return string("Female"); break;
      case UNKNOWN: return string("Unknown"); break;
   }

   return string("Really, really Unknown");
};
	

string Animal::colorName (enum Color color) {
	   switch (color) {
      case BLACK:    return string("Black"); break;
      case WHITE:  return string("White"); break;
      case RED: return string("Red"); break;
      case SILVER: return string("Silver"); break;
      case YELLOW: return string("Yellow"); break;
      case BROWN: return string("Brown"); break;

   }

   return string("Unknown");
};

const Gender Animal::getRandomGender() {
   Random randomGen;
   randomGen.setParams(s,a,c,m);
   s = randomGen.scgRand();
   enum Gender randomGender = static_cast<Gender>(s%3);
   return randomGender;

}

const Color Animal::getRandomColor() {
   Random randomCol;
   randomCol.setParams(s,a,c,m);
   s = randomCol.scgRand();
   enum Color randomColor = static_cast<Color>((s%67)%6);
   return randomColor;

}

const bool Animal::getRandomBool() {
   Random randomBoo;
   randomBoo.setParams(s,a,c,m);
   s = randomBoo.scgRand();
   bool randomBool = false;
   if((s%67)%2)
      randomBool = true;
   return randomBool;

}

const float Animal::getRandomWeight(const float from, const float to) {
   Random randomWei;
   randomWei.setParams(s,a,c,m);
   s = randomWei.scgRand();
   float randomWeight = 0;
   float range = to - from + 1;
   randomWeight = static_cast<float>((s %67) % static_cast<int>(range)) + from;
   return randomWeight;

}

const string Animal::getRandomName() {
   Random randomNa;
   randomNa.setParams(s,a,c,m);
   s = randomNa.scgRand();
   int length = ((s%67)%6) + 4;
   /*char randomName[length] = "";
   randomName[0] = 'A' + ((s%67)%26);
   int i;
   for(i = 1; i < length; i++)
   {
      s = randomNa.scgRand();
      randomName[i] = 'a' + ((s%67)%26);
   }*/
   string randomName = "";
   char rUpperLetter = 'A' + ((s%67)%26);
   randomName += rUpperLetter;
   for(int i = 1; i < length; i++)
   {
      s = randomNa.scgRand();
      char rLowerLetter = 'a' + ((s%67)%26);
      randomName += rLowerLetter;
   }
   return randomName;

}

} // namespace animalfarm
