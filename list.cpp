///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file list.cpp
/// @version 1.0
///
/// Custom made list
///
/// @author Colby Kagamida <colbykag@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   28_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "list.hpp"

using namespace std;

namespace animalfarm {

   const bool SingleLinkedList::empty() const {
      bool isEmpty = false;
      if(head == nullptr)
      {
         isEmpty = true;
      }
      return isEmpty;
   }

   void SingleLinkedList::push_front( Node* newNode ) { 
      //newNode = (Node*) malloc (sizeof(Node));
      newNode->next = head;
      head = newNode;
   }

   Node* SingleLinkedList::pop_front() {
      if(head == nullptr)
         return nullptr;
      Node* del = head;
      Node* pop = head->next;
      head = pop;
      /*while(pop != nullptr)
      {
         pop = pop->next;
      }*/
      return del;
   }

   Node* SingleLinkedList::get_first() const {
      Node* first = head;
      return first;
   }

   Node* SingleLinkedList::get_next( const Node* currentNode ) const {
      Node* nextNode = currentNode->next;
      return nextNode;
   }

   unsigned int SingleLinkedList::size() const {
      unsigned int sizeCount = 0;
      Node* counter = head;
      if(counter != nullptr){//return 0 if list is empty
         sizeCount++;
      }
      else
      {
         return sizeCount;
      }
      while(counter->next != nullptr)
      {
         counter = counter->next;
         sizeCount++;
      }//iterate over list until end of list
      return sizeCount;
   }


} // namespace animalfarm
