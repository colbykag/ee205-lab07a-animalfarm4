///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 3
///
/// @file animalFactory.hpp
/// @version 1.0
///
/// Generates random Animal
///
/// @author Colby Kagamida <colbykag@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   18_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

#include <string>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "random.hpp"


namespace animalfarm {

class AnimalFactory {
public: 
   AnimalFactory();
   static Animal* pRandomAnimal();

};

} // namespace animalfarm
