///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file bird.cpp
/// @version 1.0
///
/// Exports data about all birds
///
/// @author Colby Kagamida <colbykag@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   18_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream> 
#include <iomanip>
#include <string>

#include "bird.hpp"

using namespace std;

namespace animalfarm {
	
void Bird::printInfo() {
	Animal::printInfo();
	cout << "   Feather Color = [" << colorName( featherColor ) << "]" << endl;
   cout << boolalpha;
	cout << "   Is Migratory = [" << isMigratory << "]" << endl;
}

const string Bird::speak() {
   return string("Tweet");
}

} // namespace animalfarm
