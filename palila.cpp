///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file palila.cpp
/// @version 1.0
///
/// Exports data about all palila birds
///
/// @author Colby Kagamida <colbykag@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   18_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "palila.hpp"

using namespace std;

namespace animalfarm {

Palila::Palila(string newWhereFound, enum Color newColor, enum Gender newGender ){
   
   whereFound = newWhereFound;

   gender = newGender;

   species = "Loxioides bailleui";

   featherColor = newColor;

   isMigratory = false;
}

///Print Palila stuff then bird stuff

void Palila::printInfo() {
   cout << "Palila" << endl;
   cout << "   Where Found = [" << whereFound << "]" << endl;
   Bird::printInfo();
}

}//namespace animalfarm
