///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file node.hpp
/// @version 1.0
///
/// Node
///
/// @author Colby Kagamida <colbykag@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   28_Mar_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

using namespace std;

#include <string>

namespace animalfarm {

class Node {
protected:
   Node* next = nullptr;

   friend class SingleLinkedList;

};

} // namespace animalfarm
