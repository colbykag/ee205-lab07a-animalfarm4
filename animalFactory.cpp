///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 3
///
/// @file animalFactory.cpp
/// @version 1.0
///
/// Generate random animals
///
/// @author Colby Kagamida <colbykag@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   18_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <string>

#include "animalFactory.hpp"

using namespace std;

namespace animalfarm {

AnimalFactory::AnimalFactory(){
}

Animal* AnimalFactory::pRandomAnimal() {
   Random randomAni;
   Animal* pRanAnimal;
   randomAni.setParams(s,a,c,m);
   s = randomAni.scgRand();
   int pickRandAni = (s%67) % 6;
   switch(pickRandAni) {
      case 0:
         pRanAnimal = new Cat( pRanAnimal->getRandomName(),pRanAnimal->getRandomColor(), pRanAnimal->getRandomGender() );
         break;
      case 1:
         pRanAnimal = new Dog( pRanAnimal->getRandomName(),pRanAnimal->getRandomColor(), pRanAnimal->getRandomGender() );
         break;
      case 2:
         pRanAnimal = new Nunu( pRanAnimal->getRandomBool(),pRanAnimal->getRandomColor(), pRanAnimal->getRandomGender() );
         break;
      case 3:
         pRanAnimal = new Aku( pRanAnimal->getRandomWeight(12,18),pRanAnimal->getRandomColor(), pRanAnimal->getRandomGender() );
         break;
      case 4:
         pRanAnimal = new Palila( pRanAnimal->getRandomName(),pRanAnimal->getRandomColor(), pRanAnimal->getRandomGender() );
         break;
      default: 
         pRanAnimal = new Nene( pRanAnimal->getRandomName(), pRanAnimal->getRandomColor(), pRanAnimal->getRandomGender() );

   }
   return pRanAnimal;

}


} // namespace animalfarm
